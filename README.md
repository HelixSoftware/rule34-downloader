# What does this do?

It downloads every image it can find from rule34 based on your searches and neatly organises them into folders. 

This code was origionally for [Helix](https://gitlab.com/HelixDiscord/Helix/) but ive now modified it to download in bulk for your enjoyment ( ͡° ͜ʖ ͡°)

# Disclaimer


I do not own nor am i afiliated with rule34, this is just a script to help people get way too much porn

# How do i get this?

[Download the exe file](https://gitlab.com/HelixSoftware/rule34-downloader/raw/master/rule34.exe)

## or if you want to be nerdy and have the source code:

[Download Python](https://www.python.org/downloads/release/python-352/)

[Download the code](https://gitlab.com/HelixSoftware/rule34-downloader/repository/archive.zip?ref=master)
