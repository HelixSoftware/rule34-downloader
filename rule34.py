﻿import urllib.request, os, time, sys
try:
    from bs4 import BeautifulSoup
except ImportError:
    print("BS4 isnt on this system...")
    print("Obtaining")
    paths = sys.path
    direct = paths[4]
    direct = direct.replace("\lib","")
    direct += "\scripts\pip3.5"
    if not os.path.isfile((direct + ".py")):
        try:
            urllib.request.urlretrieve("https://bootstrap.pypa.io/get-pip.py", "get-pip.py")
            os.system("get-pip.py")
            os.unlink("get-pip.py")
        except:
            print("unable to obtain bs4, sorry this code wont work for you")

    cmd = direct + " install bs4"
    
    try:
        os.system(cmd)
        from bs4 import BeautifulSoup
        os.system("cls")
    except:
        print("unable to obtain bs4, sorry this code wont work for you")

def mainloop():
    global pid
    query = input("What am i searching for? ")
    query = query.replace(" ", "+")
    url = "https://rule34.xxx/index.php?page=dapi&s=post&q=index"
    tags = "&tags=" + query
    pid = 0
    url = url + tags
    checks(url, pid)
    if total == 0:
        print("no results")
        return
    else:
        os.system("cls")
        print("{} images found".format(total))
        if pages == 0:
            print("1 page")
        else:
            print("{} pages".format(pages))
    print("Begining Download...")
    time.sleep(2)
    download(url, query, imglist, total)
    

def validate(url, imglist):
    html = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(html, "lxml")
    url = str(soup.getText)
    url = str(url)
    url = url.split('file_url="')
    num = int(0)
    for item in url:
        test = str(item)
        if test.startswith("//img"):
            num += 1
    if num == 0:
        return(0)
    else:
        for item in url:
            test = str(item)
            url = test
            if test.startswith("//img"):
                url = url.replace('="', "")
                url = url.split(" ")
                url = url[0]
                url = url.replace('"', "")
                url = url.replace("//", "http://")
                url = url + "\n"
                imglist += url
        data = imglist + "_______________" + str(num)
        return(data)

def checks(url, pid):
    global total
    global pages
    global imglist
    imglist = ""
    print("Processing...\n\nThis can take a while")
    total = 0
    t = True
    while t == True:
        p = "&pid=" + str(pid)
        URL = url + p

        data = validate(URL, imglist)
        data = data.split("_______________")
        num = int(data[1])
        imglist = str(data[0])
        if num == 100:
            pid += 1
        else:
            t = False
        total += num
    pages = pid

def uprint(*objects, sep=' ', end='\n', file=sys.stdout):
    enc = file.encoding
    if enc == 'UTF-8':
        print(*objects, sep=sep, end=end, file=file)
    else:
        f = lambda obj: str(obj).encode(enc, errors='backslashreplace').decode(enc)
        print(*map(f, objects), sep=sep, end=end, file=file)

def download(url, query, imglist, total):
    os.system("cls")
    download = 0
    if not os.path.exists(query):
        os.makedirs(query)
    imglist = imglist.splitlines()
    for item in imglist:
        prog_bar_str = ''
        percentage = 0.0
        progress_bar = 50
        percentage = download / total
        for i in range(progress_bar):
            if (percentage < 1 / progress_bar * i):
                prog_bar_str += '_'
            else:
                prog_bar_str += '■'

        name = item.replace("/", "-")
        name = name.replace("http:--img.rule34.xxx-images-", (query + "/"))
        if not os.path.isfile(name):
            print("Downloading: " + name)
            if ".webm" in name:
                print("Downlading a webm takes a bit longer...")
            urllib.request.urlretrieve(item, name)
            download += 1
            os.system("cls")
            print("Downloaded {}/{}".format(download, total))
            uprint(prog_bar_str)
        else:
            os.system("cls")
            download += 1
            print("Downloaded {}/{}".format(download, total))
            uprint(prog_bar_str)
            print("Already downloaded " + name)
    os.system("cls")
    print("Download complete")
    print("Downloaded {}/{} images".format(download, total))
    time.sleep(5)
    exit()
     
try:        
    mainloop()
except Exception as e:
    print(e)
    time.sleep(10)
        
